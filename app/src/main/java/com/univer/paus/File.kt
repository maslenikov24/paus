package com.univer.paus

import com.google.gson.annotations.SerializedName

data class File(
    @SerializedName("Id") val id: String,
    @SerializedName("Name") val name: String?,
    @SerializedName("Content") val content: String?,
    @SerializedName("Rules") val ruleRoot: List<Rule>?

)