package com.univer.paus

object UserServiceProvider {
    fun provideRepository(): Repository {
        return Repository(ApiService.create())
    }
}