package com.univer.paus

interface ChangeSourceListener {
    fun sourceChanged(position: Int)
}