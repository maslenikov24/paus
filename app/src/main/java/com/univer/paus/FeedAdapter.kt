package com.univer.paus

import android.annotation.SuppressLint
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.*
import com.univer.paus.model.data.ApiResponse
import kotlinx.android.synthetic.main.item_user.view.*

class FeedAdapter(list: MutableList<User>): RecyclerView.Adapter<FeedAdapter.ViewHolder>() {

    private val mListeners: MutableList<ChangeSourceListener> = mutableListOf()
    private val mItems: MutableList<User> = list

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val item = mItems[position]
        holder.email.text = "Email: " + item.login
        holder.password.text = "Password: " + item.password
        holder.isAdmin.text = if (item.isAdmin) "Admin" else "Simple user"
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val view = layoutInflater.inflate(R.layout.item_user, parent, false)
        return ViewHolder(view).listen { position, type ->
            changeSource(position)
        }
    }

    override fun getItemCount(): Int {
        return mItems.size
    }

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val email = view.email!!
        val password = view.password!!
        val isAdmin = view.isAdmin!!
    }
    fun addListener(listener: ChangeSourceListener){
        mListeners.add((listener))
    }
    fun changeSource(position: Int){
        mListeners.forEach{
            it.sourceChanged(position)
        }
    }
    fun <T: RecyclerView.ViewHolder> T.listen(event: (position: Int,type:Int)->Unit):T{
        itemView.setOnClickListener{
            event.invoke(adapterPosition, itemViewType)
        }
        return this
    }
    operator fun get(position: Int): User {
        return mItems[position]
    }
}