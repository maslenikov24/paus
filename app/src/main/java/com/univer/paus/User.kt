package com.univer.paus

import com.google.gson.annotations.SerializedName

data class User(
    @SerializedName("Id") val id: String,
    @SerializedName("Login") val login: String,
    @SerializedName("Password") val password: String,
    @SerializedName("Salt") val salt: String?,
    @SerializedName("IsAdmin") val isAdmin: Boolean
)