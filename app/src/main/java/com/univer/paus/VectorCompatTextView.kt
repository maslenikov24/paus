package com.univer.paus

import android.content.Context
import android.graphics.PorterDuff
import android.graphics.drawable.Drawable
import android.os.Build
import android.support.v7.content.res.AppCompatResources
import android.util.AttributeSet
import android.widget.TextView


class VectorCompatTextView @JvmOverloads constructor(
    context: Context,
    attrs: AttributeSet? = null,
    defStyleAttr: Int = android.R.attr.textViewStyle
) : TextView(context, attrs, defStyleAttr) {

    var drawableStart: Drawable? = null
    var drawableEnd: Drawable? = null
    var drawableBottom: Drawable? = null
    var drawableTop: Drawable? = null

    init {
        if (attrs != null) {
            val attributeArray = context.obtainStyledAttributes(attrs, R.styleable.VectorCompatTextView)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                drawableStart = attributeArray.getDrawable(R.styleable.VectorCompatTextView_drawableStartCompat)
                drawableEnd = attributeArray.getDrawable(R.styleable.VectorCompatTextView_drawableEndCompat)
                drawableBottom = attributeArray.getDrawable(R.styleable.VectorCompatTextView_drawableBottomCompat)
                drawableTop = attributeArray.getDrawable(R.styleable.VectorCompatTextView_drawableTopCompat)

            } else {
                val drawableStartId = attributeArray.getResourceId(R.styleable.VectorCompatTextView_drawableStartCompat, -1)
                val drawableEndId = attributeArray.getResourceId(R.styleable.VectorCompatTextView_drawableEndCompat, -1)
                val drawableBottomId = attributeArray.getResourceId(R.styleable.VectorCompatTextView_drawableBottomCompat, -1)
                val drawableTopId = attributeArray.getResourceId(R.styleable.VectorCompatTextView_drawableTopCompat, -1)

                if (drawableStartId != -1) {
                    drawableStart = AppCompatResources.getDrawable(context, drawableStartId)
                }
                if (drawableEndId != -1) {
                    drawableEnd = AppCompatResources.getDrawable(context, drawableEndId)
                }
                if (drawableBottomId != -1) {
                    drawableBottom = AppCompatResources.getDrawable(context, drawableBottomId)
                }
                if (drawableTopId != -1) {
                    drawableTop = AppCompatResources.getDrawable(context, drawableTopId)
                }
            }

            setCompoundDrawablesWithIntrinsicBounds(drawableStart, drawableTop, drawableEnd, drawableBottom)
            attributeArray.recycle()
        }
    }

    fun setDrawableColor(color : Int) {
        drawableStart?.setColorFilter(color, PorterDuff.Mode.SRC_IN)
        drawableEnd?.setColorFilter(color, PorterDuff.Mode.SRC_IN)
        drawableBottom?.setColorFilter(color, PorterDuff.Mode.SRC_IN)
        drawableTop?.setColorFilter(color, PorterDuff.Mode.SRC_IN)
    }

}
