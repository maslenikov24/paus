package com.univer.paus

import com.univer.paus.model.data.ApiResponse
import com.univer.paus.model.data.PermissionResponse
import com.univer.paus.model.data.net.service.UserService
import io.reactivex.Completable
import io.reactivex.Observable

class Repository(private val userService: UserService) {
    fun getUser(Email: String, Password: String): Observable<ApiResponse> {
        return userService.getUser(Email, Password)
    }
    fun deleteUser(oldEmail: String): Observable<ApiResponse> {
        return userService.deleteUser(oldEmail)
    }
    fun updateUser(oldEmail: String, Email: String, isAdmin: Boolean): Observable<ApiResponse> {
        return userService.updateUser(oldEmail, Email, isAdmin)
    }
    fun addUser(Email: String, Password: String): Observable<ApiResponse> {
        return userService.addUser(Email, Password)
    }
    fun read(Login: String, File: String): Observable<ApiResponse> {
        return userService.read(Login, File)
    }
    fun write(Login: String, File: String): Observable<ApiResponse> {
        return userService.write(Login, File)
    }
    fun getPermission(): Observable<PermissionResponse> {
        return userService.getPermission()
    }
    fun getUsers(): Observable<List<User>> {
        return userService.getUsers()
    }
    fun getFiles(): Observable<List<File>> {
        return userService.getFiles()
    }
    fun setOwn(Login: String, User: String, File: String): Observable<ApiResponse>{
        return userService.setOwn(Login, User, File)
    }
    fun setTg(Login: String, User: String, File: String): Observable<ApiResponse>{
        return userService.setTg(Login, User, File)
    }
    fun setRule(Login: String, User: String, Rule: String, File: String): Observable<ApiResponse>{
        return userService.setRule(Login, User, Rule, File)
    }
}