package com.univer.paus

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity;
import android.text.InputType
import android.view.MenuItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_user_profile.*

const val INTENT_ID: String = "id"
const val INTENT_EMAIL: String = "go@google.com"
const val INTENT_PASSWORD: String = "pass"
const val INTENT_IS_ADMIN: String = "isAdmin"
class UserProfile : AppCompatActivity() {

    private var shown = false

    val compositeDisposable:CompositeDisposable = CompositeDisposable()
    val repository: Repository = UserServiceProvider.provideRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_user_profile)

        var email_ = intent.getStringExtra(INTENT_EMAIL)
        var password_ = intent.getStringExtra(INTENT_PASSWORD)
        var isAdmin_ = intent.getBooleanExtra(INTENT_IS_ADMIN, false)


        login.text = email_
        password.setText(password_)
        isAdmin.text = isAdmin_.toString()
        //val actionBar = supportActionBar
        //actionBar!!.setHomeButtonEnabled(true)
        //actionBar.setDisplayHomeAsUpEnabled(true)
        //app_password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
        password.keyListener = null

        visible.setOnClickListener{
            if (!shown){
                //app_password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                visible.setImageResource(R.drawable.ic_visible_off)
                //app_password.inputType = InputType.TYPE_CLASS_TEXT
                //app_password.inputType = InputType.TYPE_NULL
                shown = true
            }
            else {
                visible.setImageResource(R.drawable.ic_visible_on)
                //app_password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                password.keyListener = null
                shown = false
            }
        }

        edit.setOnClickListener{
            val intent = Intent(this@UserProfile, UserChange::class.java)
            intent
                .putExtra(INTENT_IS_ADMIN, isAdmin.text.toString().toBoolean())
                .putExtra(INTENT_EMAIL, login.text as String?)
                .putExtra(INTENT_PASSWORD, password_)
            startActivityForResult(intent, 1)
        }
        delete.setOnClickListener{
            AlertDialog
                .Builder(this@UserProfile)
                .setTitle("Удаление учётной записи")
                .setMessage("Вы действительно хотите удалить учётную запись?")
                .setPositiveButton("Да") { _, _ -> delete(/*TODO(запрос на выпиливание)*/"123", email_) }
                .setNegativeButton("Отмена") { _, _ -> }
                .create().show()
        }
    }

    fun delete(name: String, email: String){
            compositeDisposable.add(repository.deleteUser(email)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate { startActivity(Intent(this@UserProfile, Feed::class.java)
                    .putExtra("email", Feed.fix.email)
                    .putExtra("pass", Feed.fix.password)) }
                .subscribe({},{})
            )
        /*TODO(delete this)*/
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (data!=null){
            login.text = data.getStringExtra(INTENT_EMAIL)
            password.text = data.getStringExtra(INTENT_PASSWORD)
            isAdmin.text = data.getBooleanExtra(INTENT_IS_ADMIN, false).toString()
        }
    }
    /*override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                this.finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }*/


}
