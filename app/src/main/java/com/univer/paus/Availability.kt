package com.univer.paus

import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.PopupMenu
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.View
import android.widget.*
import kotlinx.android.synthetic.main.fragment_availability.*
import com.univer.paus.extentions.visible
import com.univer.paus.model.data.PermissionResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class Availability : AppCompatActivity() {

    var permis = ""
    var userLogin = ""
    var choosefile = ""
    var list = mutableListOf<String>()
    var listUser = mutableListOf<User>()
    var listFiles = mutableListOf<File>()
    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val repository: Repository = UserServiceProvider.provideRepository()
    val listUserOthers = mutableListOf<User>()
    val listMyFiles = mutableListOf<File>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_availability)
        //getResponse()

        /*listFiles.add(File("1", "f1", null, mutableListOf(Rule("u1", "own"), Rule("u2", "r"))))
        listFiles.add(File("2", "f2", null, mutableListOf(Rule("u3", "own"), Rule("u1", "rw"))))
        listFiles.add(File("2", "f3", null, mutableListOf(Rule("u2", "own"), Rule("u3", "rw"))))
        listFiles.add(File("2", "f4", null, mutableListOf(Rule("u1", "r"), Rule("u3", "w"))))

        listUser.add(User("", "u1", "", null, false))
        listUser.add(User("", "u2", "", null, false))
        listUser.add(User("", "u3", "", null, false))*/

        seekBar.max = 10
        seekBar.progress = 5

        seekBar.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener{
            override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
                counterOfPermission.text = progress.toString()
            }

            override fun onStartTrackingTouch(seekBar: SeekBar?) {
            }

            override fun onStopTrackingTouch(seekBar: SeekBar?) {
            }

        })

        users.setOnClickListener {
            PopupMenu(this@Availability, it).run {
                listMyFiles.forEachIndexed {index, file ->
                    menu.add(Menu.NONE, index, Menu.NONE, file.name)
                }

                setOnMenuItemClickListener { menuItem ->
                    textChooseLevel.visible(true)
                    seekBar.visible(true)
                    counterOfPermission.visible(true)
                    allow.visible(true)
                    val index = menuItem.itemId
                    users.text = listMyFiles[index].name
                    choosefile = listMyFiles[index].name?: ""

                    listMyFiles[index].ruleRoot?.forEach{ it ->
                            if (it.login == userLogin){
                                when {
                                    it.rule == "OWN" -> {
                                        permis = "OWN"
                                        allow.visible(true)
                                    }
                                    it.rule == "RW" -> {
                                        permis = "RW"
                                        allow.visible(false)
                                    }
                                    it.rule == "TGRW" -> {
                                        permis = "TGRW"
                                        allow.visible(true)
                                    }
                                    it.rule == "W" -> {
                                        permis = "W"
                                        allow.visible(false)
                                    }
                                    it.rule == "TGW" -> {
                                        permis = "TGW"
                                        allow.visible(true)
                                    }
                                    it.rule == "R" -> {
                                        permis = "R"
                                        allow.visible(false)
                                    }
                                    it.rule == "TGR" -> {
                                        permis = "TGR"
                                        allow.visible(true)
                                    }
                                }
                            }
                        }


                    true
                }
                show()
            }
        }


    }

    fun getLists(){
        userLogin = intent.getStringExtra("email")
        //userLoginText.text = userLogin
        //if (userLogin == "root") btnPermission.visible(true)
        //btnPermission.setOnClickListener {
        //    startActivity(Intent(applicationContext, Table::class.java))
        //}

        for (user in listUser){
            if (user.login != userLogin) listUserOthers.add(user)
        }

        for (file in listFiles) {
            file.ruleRoot?.forEach { it ->
                if (it.login == userLogin) {
                    if (!it.rule.isNullOrEmpty()) listMyFiles.add(file)
                }
            }
        }
    }

    private fun getResponse(){
        compositeDisposable.add(repository.getPermission()
            .subscribeOn(Schedulers.io())
            .doAfterTerminate {

            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                setData(it)
            },{
                Toast.makeText(this@Availability, it.toString(), Toast.LENGTH_SHORT).show()
            })
        )
    }

    private fun setData(data: PermissionResponse){
        listFiles.addAll(data.files.orEmpty())
        listUser.addAll(data.users.orEmpty())
        getLists()
    }
}
