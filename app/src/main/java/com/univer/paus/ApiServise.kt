package com.univer.paus

import com.univer.paus.model.data.ApiResponse
import com.univer.paus.model.data.net.service.UserService
import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query


interface ApiService {


    companion object Factory {
        fun create(): UserService {
            //val gson: Gson = GsonBuilder().setLenient().create()
            val retrofit = Retrofit.Builder()
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                //.addConverterFactory(GsonConverterFactory.create(gson))
                .addConverterFactory(GsonConverterFactory.create())
                .baseUrl("http://192.168.43.11:3001/")
                .build()
            return retrofit.create(UserService::class.java)
        }
    }
}