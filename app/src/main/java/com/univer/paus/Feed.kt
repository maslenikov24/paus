package com.univer.paus

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_feed.*

class Feed : AppCompatActivity(), ChangeSourceListener {

    override fun onBackPressed() {
            AlertDialog.Builder(this@Feed)
                .setTitle("Выйти?")
                .setPositiveButton("Да") { _, _ ->  super.onBackPressed()}
                .setNegativeButton("Отмена") { _, _ -> }
                .create().show()
    }

    override fun sourceChanged(position: Int) {
        val intent = Intent(this, UserProfile::class.java)
        val l = list
        intent
            .putExtra(INTENT_EMAIL, list[position].login)
            .putExtra(INTENT_PASSWORD, list[position].password)
            .putExtra(INTENT_IS_ADMIN, list[position].isAdmin)
        startActivity(intent)
    }

    private lateinit var mRunnable:Runnable
    var list: MutableList<User> = mutableListOf()
    val compositeDisposable:CompositeDisposable = CompositeDisposable()
    val repository: Repository = UserServiceProvider.provideRepository()
    lateinit var adapter: FeedAdapter

    object fix{
        var email = ""
        var password = ""
    }

    fun refreshThisPage() {
        refresh.setColorSchemeResources(R.color.colorPrimary)
        refresh.setOnRefreshListener {
            mRunnable = Runnable {
                finish()
                startActivity(intent)
                refresh.isRefreshing = true
            }
            Handler(Looper.getMainLooper()).postDelayed(
                mRunnable,
                (3).toLong()
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_feed)
        refreshThisPage()
        val email = intent.getStringExtra("email")
        val password = intent.getStringExtra("pass")
        fix.email = email
        fix.password = password

        /*list.add(User(id = "", login = "aaa", password = "f4W", isAdmin = false, salt = null))
        list.add(User(id = "", login = "ббб", password = "r9h", isAdmin = false, salt = null))
        list.add(User(id = "", login = "ввв", password = "bkg0", isAdmin = false, salt = null))
        list.add(User(id = "", login = "ггг", password = "gd1n", isAdmin = true, salt = null))
        list.add(User(id = "", login = "ддд", password = "kdk3", isAdmin = false, salt = null))

        recyclerView.apply {
            layoutManager = LinearLayoutManager(this@Feed)
            val adapterIn = FeedAdapter(list)
            adapterIn.addListener(this@Feed)
            adapter = adapterIn
            setHasFixedSize(true)
        }*/

        compositeDisposable.add(
            repository.getUser(email, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    {response ->
                        recyclerView.apply {
                            layoutManager = LinearLayoutManager(this@Feed)
                            list = response.data!!.toMutableList()
                            val adapterIn = response.data?.toMutableList()?.let { FeedAdapter(it) }
                            adapterIn?.addListener(this@Feed)
                            adapter = adapterIn
                            //adapter = FeedAdapter(listIn)
                            setHasFixedSize(true)
                        }
                    },
                    {}
                )
        )

    }
}
