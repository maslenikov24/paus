package com.univer.paus

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.univer.paus.extentions.visible
import com.univer.paus.model.data.ApiResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.item_authorization.*

class Authorization : AppCompatActivity() {

    private var isAuth = true
    private val list: MutableList<ApiResponse> = mutableListOf()
    private val compositeDisposable:CompositeDisposable = CompositeDisposable()
    private val repository: Repository = UserServiceProvider.provideRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_authorization)
        //startActivity(Intent(applicationContext, Table::class.java))
        //startActivity(Intent(applicationContext, MyFiles::class.java))
        startActivity(Intent(applicationContext, Availability::class.java))
        action.setOnClickListener {
            when {
                isAuth -> getResponse(email.text.toString(), password.text.toString())
                repeatPassword.text.toString() == password.text.toString() -> getResponse2(email.text.toString(), password.text.toString())
                else -> Toast.makeText(applicationContext, "Пароли не совпадают", Toast.LENGTH_SHORT).show()
            }
        }
        sing_up.setOnClickListener{
            isAuth = false
            repeatPassword.visible(true)
            sing_in.visible(true)
            it.visible(false)
            log_in.text = getString(R.string.sign_up)
            action.text = getString(R.string.sign_up)
        }
        sing_in.setOnClickListener{
            isAuth = true
            repeatPassword.visible(false)
            sing_up.visible(true)
            it.visible(false)
            log_in.text = getString(R.string.sign_in)
            action.text = getString(R.string.sign_in)
        }
    }

    private fun getResponse(email: String, password: String) {
        compositeDisposable.add(
            repository.getUser(email, password) // Метод из сервиса
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doAfterTerminate {
                    /*if(!list[0].data.isNullOrEmpty()) {
                        startActivity(Intent(this, Table::class.java))
                        /*startActivity(Intent(this, Feed::class.java)
                       .putExtra("email", email).putExtra("pass", password))*/
                    }
                    else */if (list[0].isSuccess == true){
                        startActivity(Intent(this, MyFiles::class.java)
                            .putExtra("email", email))
                        /*val intent = Intent(this, UserProfile::class.java)
                        intent
                            .putExtra(INTENT_EMAIL, email)
                            .putExtra(INTENT_PASSWORD, password)
                            .putExtra(INTENT_IS_ADMIN, false)
                        startActivity(intent)*/
                    }
                    else Toast.makeText(applicationContext, "Wrong Password!", Toast.LENGTH_SHORT).show()
                    list.clear()
                }
                .subscribe ({
                    list.add(it)
                    Log.d("Data", list.toString())
                },{
                    Toast.makeText(applicationContext, "Response is Empty", Toast.LENGTH_SHORT).show()
                }))
    }

    private fun getResponse2(email1: String, password1: String) {
        compositeDisposable.add(
            repository.addUser(email1, password1) // Метод из сервиса
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .doAfterTerminate {
                    Toast.makeText(applicationContext, "Пользователь зарегистрирован", Toast.LENGTH_SHORT).show()
                    isAuth = true
                    repeatPassword.visible(false)
                    sing_up.visible(true)
                    sing_in.visible(false)
                    email.setText("")
                    password.setText("")
                    repeatPassword.setText("")
                    log_in.text = getString(R.string.sign_in)
                    action.text = getString(R.string.sign_in)}
                .subscribe({

                },{

                })
        )
    }
}
