package com.univer.paus

import android.content.Intent
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.text.InputType
import android.view.MenuItem
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers
import kotlinx.android.synthetic.main.fragment_user_profile_change.*
import javax.crypto.Cipher

/*const val INTENT_ID : String = "id"
const val INTENT_ICON_CHANGE: String = "Here"
const val INTENT_NAME_CHANGE: String = "Google"
const val INTENT_EMAIL_CHANGE: String = "go@google.com"
const val INTENT_PASSWORD_CHANGE: String = "pass"*/
class UserChange : AppCompatActivity() {

    private var shown = false
    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val repository: Repository = UserServiceProvider.provideRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_user_profile_change)

        //val actionBar = supportActionBar
        //actionBar!!.setHomeButtonEnabled(true)
        //actionBar.setDisplayHomeAsUpEnabled(true)
        //password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD

        val isAdmin_ = intent.getBooleanExtra(INTENT_IS_ADMIN, false)
        val email_ = intent.getStringExtra(INTENT_EMAIL)
        val password_ = intent.getStringExtra(INTENT_PASSWORD)

        login.setText(email_)
        password.text = password_
        isAdmin.setText(isAdmin_.toString())

        submit.setOnClickListener{
            //TODO(Edit)
            val completeble = repository.updateUser(email_, login.text.toString(), isAdmin.text.toString().toBoolean())
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doAfterTerminate{
                    val intent = Intent(this, Feed::class.java)
                        .putExtra(INTENT_IS_ADMIN, isAdmin.text.toString().toBoolean())
                        .putExtra(INTENT_EMAIL, login.text.toString())
                        .putExtra(INTENT_PASSWORD, password_)
                    setResult(1, intent)
                    finish()
                }
                .subscribe( {

                },{}
                )
        }

        /*visible.setOnClickListener{
            if (!shown){
                password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                visible.setImageResource(R.drawable.ic_visible_off)
                password.inputType = InputType.TYPE_CLASS_TEXT
                shown = true
            }
            else {
                visible.setImageResource(R.drawable.ic_visible_on)
                password.inputType = InputType.TYPE_CLASS_TEXT or InputType.TYPE_TEXT_VARIATION_PASSWORD
                shown = false
            }
        }*/


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            android.R.id.home -> {
                this.finish()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

}
