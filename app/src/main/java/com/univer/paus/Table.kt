package com.univer.paus

import android.content.res.Resources
import android.graphics.Color
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_table.*
import android.widget.TableRow
import android.widget.TextView
import com.univer.paus.model.data.PermissionResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class Table : AppCompatActivity() {

    //region ===block Todp===
    val Int.px: Int
        get() = (this / Resources.getSystem().displayMetrics.density).toInt()
    val Int.dp: Int
        get() = (this * Resources.getSystem().displayMetrics.density).toInt()
    val Float.dp: Float
        get() = (this * Resources.getSystem().displayMetrics.density).toFloat()
    //endregion

    var adapter: DataAdapter? = null
    var list = mutableListOf<String>()
    var listUser = mutableListOf<User>()
    var listFiles = mutableListOf<File>()
    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val repository: Repository = UserServiceProvider.provideRepository()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_table)


        getResponse()
        //getUsers()
        //getFiles()
        /*listFiles.add(File("1", "f1", null, mutableListOf(Rule("u1", "own"), Rule("u2", "r"))))
        listFiles.add(File("2", "f2", null, mutableListOf(Rule("u3", "own"), Rule("u1", "wr"))))
        listFiles.add(File("2", "f3", null, mutableListOf(Rule("u2", "own"), Rule("u1", "w"), Rule("u3", "rw"))))

        listUser.add(User("", "u1", "", null, false))
        listUser.add(User("", "u2", "", null, false))
        listUser.add(User("", "u3", "", null, false))*/
        tableBuild()
    }

    private fun getResponse(){
        compositeDisposable.add(repository.getPermission()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                setData(it)
            },{
                Toast.makeText(this@Table, it.toString(), Toast.LENGTH_SHORT).show()
            })
        )
    }

    private fun setData(data: PermissionResponse){
        listFiles.addAll(data.files.orEmpty())
        listUser.addAll(data.users.orEmpty())
        tableBuild()
    }

    private fun tableBuild(){
        val tableRowHeader = TableRow(this)
        tableRowHeader.layoutParams = TableRow.LayoutParams(
            TableRow.LayoutParams.MATCH_PARENT,
            TableRow.LayoutParams.MATCH_PARENT)
        var textView = TextView(this)
        var countF = 0
        tableRowHeader.addView(textView, 0)

        for (i in listFiles){
            textView = TextView(this)
            textView.setTextColor(Color.BLACK)
            textView.text = listFiles[countF].name
            textView.gravity = Gravity.CENTER
            textView.textSize = 4.toFloat().dp
            textView.typeface = Typeface.DEFAULT_BOLD
            tableRowHeader.addView(textView, countF+1)
            countF ++
        }
        table.addView(tableRowHeader, 0)
        var countU=0

        for(i in listUser){
            val tableRow = TableRow(this)
            tableRow.layoutParams = TableRow.LayoutParams(
                TableRow.LayoutParams.MATCH_PARENT,
                TableRow.LayoutParams.MATCH_PARENT)
            countF=0
            textView = TextView(this)
            textView.text = listUser[countU].login
            textView.setTextColor(Color.BLACK)
            textView.gravity = Gravity.CENTER
            textView.textSize = 4.toFloat().dp
            textView.typeface = Typeface.DEFAULT_BOLD
            tableRow.addView(textView, countF)
            for (file in listFiles){
                textView = TextView(this)
                textView.textSize = 4.toFloat().dp
                textView.setTextColor(Color.BLACK)
                var flag = false
                for (item in file.ruleRoot.orEmpty()){
                    val c = item
                    if (item.login == listUser[countU].login) {
                        flag = true
                        if (item.rule.isNullOrEmpty()) textView.text = "-"
                        else textView.text = item.rule
                        textView.gravity = Gravity.CENTER
                        tableRow.addView(textView, countF + 1)
                        countF++
                        break
                    }
                }
                /*if (!flag) {
                    textView.text = "-"
                    textView.gravity = Gravity.CENTER
                    tableRow.addView(textView, countF + 1)
                    countF++
                }*/
            }
            countU++
            table.addView(tableRow, countU)
        }
    }

    private fun getUsers(){
        compositeDisposable.add(repository.getUsers()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                setData(it)
            },{
                Toast.makeText(this@Table, it.toString(), Toast.LENGTH_SHORT).show()
            })
        )
    }

    private fun getFiles(){
        compositeDisposable.add(repository.getFiles()
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                setData(it)
            },{
                Toast.makeText(this@Table, it.toString(), Toast.LENGTH_SHORT).show()
            })
        )
    }

    private fun setData(data: Any){
        if (data is List<*>){
            for (item in data){
                if (item is User) listUser.addAll(data as List<User>)
                else if (item is File) listFiles.addAll(data as List<File>)
            }
        }
        //listFiles.addAll(data.files.orEmpty())
        //listUser.addAll(data.users.orEmpty())
        tableBuild()
    }
}
