package com.univer.paus.model.data.net

import com.google.gson.annotations.SerializedName

data class Authorization(
    @SerializedName("Login") val login: String,
    @SerializedName("Password") val password: String
)