package com.univer.paus.model.repository

import android.widget.Toast
import com.univer.paus.Repository
import com.univer.paus.User
import com.univer.paus.UserServiceProvider
import io.reactivex.Single
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class UserRepository (){

    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val repository: Repository = UserServiceProvider.provideRepository()

    /*private fun getResponse() :Single<List<User>> = Single
        .defer{
            compositeDisposable.add(repository.getPermission()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    val a = it
                    Single.just(it)
                },{
                    // Toast.makeText(this@MyFiles, it.toString(), Toast.LENGTH_SHORT).show()
                })
            )
        }
*/
}