package com.univer.paus.model.data

import com.google.gson.annotations.SerializedName
import com.univer.paus.User

data class ApiResponse(
    @SerializedName("Success") val isSuccess: Boolean? = null,
    @SerializedName("Data") val data: List<User>?
)