package com.univer.paus.model.data.net.service

import com.univer.paus.File
import com.univer.paus.User
import com.univer.paus.model.data.ApiResponse
import com.univer.paus.model.data.PermissionResponse
import io.reactivex.Completable
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query
interface UserService{
    @GET("api/authorization")
    fun getUser(@Query("Login") email: String,
                @Query("Password") password: String): Observable<ApiResponse>

    @GET("api/add")
    fun addUser(@Query("Login") email: String,
                @Query("Password") password: String): Observable<ApiResponse>

    @GET("api/update")
    fun updateUser(@Query("Login") oldEmail: String,
                   @Query("newLogin") email: String,
                   @Query("IsAdmin") isAdmin: Boolean): Observable<ApiResponse>

    @GET("api/delete")
    fun deleteUser(@Query("Login") oldEmail: String): Observable<ApiResponse>

    @GET("api/read")
    fun read(@Query("Login") oldEmail: String,
                   @Query("File") email: String): Observable<ApiResponse>

    @GET("api/write")
    fun write(@Query("Login") oldEmail: String,
             @Query("File") email: String): Observable<ApiResponse>

    @GET("api/")
    fun getPermission(): Observable<PermissionResponse>

    @GET("api/users")
    fun getUsers(): Observable<List<User>>

    @GET("api/files")
    fun getFiles(): Observable<List<File>>

    @GET("api/setOWN")
    fun setOwn(@Query("Login") oldEmail: String,
               @Query("User") user: String,
               @Query("File") email: String): Observable<ApiResponse>

    @GET("api/setTG")
    fun setTg(@Query("Login") oldEmail: String,
              @Query("User") user: String,
              @Query("File") email: String): Observable<ApiResponse>

    @GET("api/setRule")
    fun setRule(@Query("Login") oldEmail: String,
                @Query("User") user: String,
                @Query("Rule") rule: String,
                @Query("File") email: String): Observable<ApiResponse>
}
