package com.univer.paus.model.data

import com.google.gson.annotations.SerializedName
import com.univer.paus.File
import com.univer.paus.User

data class PermissionResponse(
    @SerializedName("USERS") val users: List<User>?,
    @SerializedName("FILES") val files: List<File>?
)