package com.univer.paus

import com.google.gson.annotations.SerializedName

data class Rule(
    @SerializedName("Login") val login: String?,
    @SerializedName("Rule") val rule : String?

)