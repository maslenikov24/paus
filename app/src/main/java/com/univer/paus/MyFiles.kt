package com.univer.paus

import android.content.Intent
import android.content.res.Resources
import android.graphics.Color
import android.graphics.Typeface
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.PopupMenu
import android.util.Log
import android.view.Gravity
import android.view.Menu
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_my_files.*
import android.widget.TableRow
import android.widget.TextView
import com.univer.paus.extentions.visible
import com.univer.paus.model.data.PermissionResponse
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers


class MyFiles : AppCompatActivity() {

    var permis = ""
    var userLogin = ""
    var choosefile = ""
    var list = mutableListOf<String>()
    var listUser = mutableListOf<User>()
    var listFiles = mutableListOf<File>()
    val compositeDisposable: CompositeDisposable = CompositeDisposable()
    val repository: Repository = UserServiceProvider.provideRepository()
    val listUserOthers = mutableListOf<User>()
    val listMyFiles = mutableListOf<File>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_my_files)
        getResponse()

        /*listFiles.add(File("1", "f1", null, mutableListOf(Rule("u1", "own"), Rule("u2", "r"))))
        listFiles.add(File("2", "f2", null, mutableListOf(Rule("u3", "own"), Rule("u1", "rw"))))
        listFiles.add(File("2", "f3", null, mutableListOf(Rule("u2", "own"), Rule("u3", "rw"))))
        listFiles.add(File("2", "f4", null, mutableListOf(Rule("u1", "r"), Rule("u3", "w"))))

        listUser.add(User("", "u1", "", null, false))
        listUser.add(User("", "u2", "", null, false))
        listUser.add(User("", "u3", "", null, false))*/

        create.setOnClickListener {

        }

        read.setOnClickListener {
            compositeDisposable.add(repository.read(userLogin, choosefile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Toast.makeText(this@MyFiles, it.isSuccess.toString(), Toast.LENGTH_SHORT).show()
                },{
                    Toast.makeText(this@MyFiles, it.toString(), Toast.LENGTH_SHORT).show()
                })
            )
        }

        write.setOnClickListener {
            compositeDisposable.add(repository.write(userLogin, choosefile)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    Toast.makeText(this@MyFiles, it.isSuccess.toString(), Toast.LENGTH_SHORT).show()
                },{
                    Toast.makeText(this@MyFiles, it.toString(), Toast.LENGTH_SHORT).show()
                })
            )
        }

        allow.setOnClickListener {
            when (permis) {
                "OWN" -> o()
                "TGR" -> tg("R")
                "TGW" -> tg("W")
                "TGRW" -> tg("RW")
            }
        }

        files.setOnClickListener {
            PopupMenu(this@MyFiles, it).run {
                listMyFiles.forEachIndexed {index, file ->
                    menu.add(Menu.NONE, index, Menu.NONE, file.name)
                }

                setOnMenuItemClickListener { menuItem ->
                    val index = menuItem.itemId
                    files.text = listMyFiles[index].name
                    choosefile = listMyFiles[index].name?: ""
                    //Toast.makeText(this@MyFiles, listFiles[index].name, Toast.LENGTH_SHORT).show()

                    listMyFiles[index].ruleRoot?.forEach{ it ->
                            if (it.login == userLogin){
                                when {
                                    it.rule == "OWN" -> {
                                        permis = "OWN"
                                        read.visible(true)
                                        write.visible(true)
                                        textPermissionTo.visible(true)
                                        authors.visible(true)
                                        allow.visible(true)
                                    }
                                    it.rule == "RW" -> {
                                        permis = "RW"
                                        textPermissionTo.visible(false)
                                        read.visible(true)
                                        write.visible(true)
                                        authors.visible(false)
                                        allow.visible(false)
                                    }
                                    it.rule == "TGRW" -> {
                                        permis = "TGRW"
                                        textPermissionTo.visible(true)
                                        read.visible(true)
                                        write.visible(true)
                                        authors.visible(true)
                                        allow.visible(true)
                                    }
                                    it.rule == "W" -> {
                                        permis = "W"
                                        textPermissionTo.visible(false)
                                        write.visible(true)
                                        read.visible(false)
                                        authors.visible(false)
                                        allow.visible(false)
                                    }
                                    it.rule == "TGW" -> {
                                        permis = "TGW"
                                        textPermissionTo.visible(true)
                                        write.visible(true)
                                        read.visible(false)
                                        authors.visible(true)
                                        allow.visible(true)
                                    }
                                    it.rule == "R" -> {
                                        permis = "R"
                                        textPermissionTo.visible(false)
                                        read.visible(true)
                                        write.visible(false)
                                        authors.visible(false)
                                        allow.visible(false)
                                    }
                                    it.rule == "TGR" -> {
                                        permis = "TGR"
                                        textPermissionTo.visible(true)
                                        read.visible(true)
                                        write.visible(false)
                                        authors.visible(true)
                                        allow.visible(true)
                                    }
                                }
                            }
                        }


                    true
                }
                show()
            }
        }
        authors.setOnClickListener {
            PopupMenu(this@MyFiles, it).run {
                listUserOthers.forEachIndexed {index, file ->
                    menu.add(Menu.NONE, index, Menu.NONE, file.login)
                }

                setOnMenuItemClickListener { menuItem ->
                    val index = menuItem.itemId
                    authors.text = listUserOthers[index].login
                    //Toast.makeText(this@MyFiles,listUserOthers[index].login, Toast.LENGTH_SHORT).show()
                    true
                }
                show()
            }
        }

    }

    fun o(){
        compositeDisposable.add(repository.setOwn(Login = userLogin, File = choosefile, User = authors.text.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Toast.makeText(this@MyFiles, it.isSuccess.toString(), Toast.LENGTH_SHORT).show()
            },{
                Toast.makeText(this@MyFiles, it.toString(), Toast.LENGTH_SHORT).show()
            })
        )
    }

    fun tg(rule: String){
        compositeDisposable.add(repository.setRule(Login = userLogin, File = choosefile, User = authors.text.toString(), Rule = rule)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Toast.makeText(this@MyFiles, it.isSuccess.toString(), Toast.LENGTH_SHORT).show()
            },{
                Toast.makeText(this@MyFiles, it.toString(), Toast.LENGTH_SHORT).show()
            })
        )
    }

    fun tgrw(){
        compositeDisposable.add(repository.setTg(Login = userLogin, File = choosefile, User = authors.text.toString())
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                Toast.makeText(this@MyFiles, it.isSuccess.toString(), Toast.LENGTH_SHORT).show()
            },{
                Toast.makeText(this@MyFiles, it.toString(), Toast.LENGTH_SHORT).show()
            })
        )
    }

    fun getLists(){
        userLogin = intent.getStringExtra("email")
        userLoginText.text = userLogin
        if (userLogin == "root") btnPermission.visible(true)
        btnPermission.setOnClickListener {
            startActivity(Intent(applicationContext, Table::class.java))
        }

        for (user in listUser){
            if (user.login != userLogin) listUserOthers.add(user)
        }

        for (file in listFiles) {
            file.ruleRoot?.forEach { it ->
                if (it.login == userLogin) {
                    if (!it.rule.isNullOrEmpty()) listMyFiles.add(file)
                }
            }
        }
    }

    private fun getResponse(){
        compositeDisposable.add(repository.getPermission()
            .subscribeOn(Schedulers.io())
            .doAfterTerminate {

            }
            .observeOn(AndroidSchedulers.mainThread())
            .subscribe({
                setData(it)
            },{
                Toast.makeText(this@MyFiles, it.toString(), Toast.LENGTH_SHORT).show()
            })
        )
    }

    private fun setData(data: PermissionResponse){
        listFiles.addAll(data.files.orEmpty())
        listUser.addAll(data.users.orEmpty())
        getLists()
    }
}
